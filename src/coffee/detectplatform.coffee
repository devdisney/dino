$ ->
	md = new MobileDetect(window.navigator.userAgent)

	if md.os() == 'AndroidOS'
		$('body').addClass 'AndroidOS notiOS'
	else if md.os() == 'iOS'
		$('body').addClass 'iOS notAndroidOS'

	if md.mobile()
		$('body').addClass 'mobile'