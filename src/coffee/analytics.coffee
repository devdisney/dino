$ ->
    # 'click touchend' = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone/i.test(navigator.userAgent) ) ? 'touchend' : 'click'
    $(document).on 'click touchend', "#playBtn", ->
        console.log "send"
        ga("send", "event", "Videos", "play", "Dinosaur Trailer 1 Main") if typeof ga is "function"
        return

    $(document).on 'click touchend', "#pay-1", ->
        console.log "send"
        ga("send", "event", "Tickets", "buy", "Dinosaur Main") if typeof ga is "function"
        return

    $(document).on 'click touchend', "#pay-2", ->
        console.log "send"
        ga("send", "event", "Tickets", "buy", "Dinosaur Main") if typeof ga is "function"
        return
    
    $(document).on 'click touchend', "#open-trailers", ->
        console.log "send"
        ga("send", "event", "Content", "viewed", "Dinosaur Trailers") if typeof ga is "function"
        return
    
    $(document).on 'click touchend', ".link-an-send", ->
        console.log "send"
        ga("send", "event", "Games", "download", "Dinosaur") if typeof ga is "function"
        return  
    
    $(document).on 'click touchend', "#contest-an-send", ->
        console.log "send"
        ga("send", "event", "Links", "click", "Dinosaur Contest") if typeof ga is "function"
        return

    $(document).on 'click touchend', ".trailers__item_send", ->
        index = $(@).data "index"
        console.log "send #{index}"
        ga("send", "event", "Videos", "play", "Dinosaur Trailer #{index}") if typeof ga is "function"
        return
    
    $(document).on 'click touchend', ".handmade-send", ->
        console.log "send"
        index = $(@).data "index"
        ga("send", "event", "Content", "download", "Dinosaur Activities Slot #{index}") if typeof ga is "function"
        return  

    $(document).on 'click touchend', ".socials--fb", ->
        console.log "send"
        ga('send', 'event', 'Content', 'share', 'Facebook') if typeof ga is "function"
        return

    $(document).on 'click touchend', ".socials--tw", ->
        console.log "send"
        ga('send', 'event', 'Content', 'share', 'Twitter') if typeof ga is "function"
        return

    $(document).on 'click touchend', ".socials--ok", ->
        console.log "send"
        ga('send', 'event', 'Content', 'share', 'OK') if typeof ga is "function"
        return

    $(document).on 'click touchend', '#nav a', ->
        ind = $(@).parents('li').index() + 1
        console.log ind
        ga('send', 'event', 'Menu', 'click', "Position #{ind}") if typeof ga is "function"