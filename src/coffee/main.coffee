$ ->

    keyControl = (el) ->
        $(document).on 'keyup', (e) ->
            if e.which == 39
                type = 'next'
            else if e.which == 37
                type = 'prev'
            el.trigger(type+'.owl.carousel')

    keyControl($('.keycontrol'))