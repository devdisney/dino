### 
    Init owlCarousel with custom DOM options
    
    http://www.owlcarousel.owlgraphic.com/docs/api-options.html
    @PluginCreateCarousel-options 
    @responsive-options
    @nav-text
   
    https://github.com/smashingboxes/OwlCarousel2
###
+ do ($ = jQuery, window) ->
    'use strict';
    class CreateCarousel
        constructor: (element) ->
            @.el = $(element)
            @.options = {
                loop: false
                nav: false
                responsive: {}
                navText: []
                freeDrag: false
            }

        build: ->
            $this = @.el
            opt = @.extendOptions()

            if typeof $.fn.owlCarousel == 'function'
                $this.owlCarousel opt

                sendanalytics $this

            else
                console.log 'You must install owlCarousel https://github.com/smashingboxes/OwlCarousel2'

        getOptions: ->
            $this = @.el
            optionsObj = {
                domOptions: $this.data('plugin-options')
                responsiveDomOptions: $this.data('responsive-options')
                navText: $this.data('nav-text')
            }
    

        extendOptions: ->
            $this = @.el
            $options = @.options
            $optionsObj = @.getOptions()
            
            if $optionsObj? and Object.keys($optionsObj).length != 0
                $.extend $options, $optionsObj.domOptions

            if  $optionsObj.responsiveDomOptions? and Object.keys($optionsObj.responsiveDomOptions).length != 0
                # opts = prepareResponseOption $optionsObj.responsiveDomOptions
                # $.extend true, $options.responsive, opts
                $.extend true, $options.responsive, $optionsObj.responsiveDomOptions

            if $optionsObj.navText?
                $.extend true, $options.navText, $optionsObj.navText.split(',')

            return $options

        destroy: ->
            $(@el).trigger 'destroy.owl.carousel'

        sendanalytics = (carousel) ->
            # if $(carousel).hasClass 'sendanalytics'
            #     string = $(carousel).data 'string'
            #     carousel.on 'translated.owl.carousel', (e) ->
            #         index = e.page.index + 1
            #         console.log "#{string} #{index}" if typeof ga is 'function'
            #         ga("send", "event" ,"Content", "viewed", "#{string} #{index}") if typeof ga is "function"

        prepareResponseOption = (opts) ->
            for key, value of opts
              opts[key] = { 
                items: value
              }

            return opts

    
    class CarouselPopup
        constructor: (element) ->
            @el = element

        showPopup: ->
            $(@el).addClass 'show'

        hidePopup: ->
            $(@el).removeClass 'show'

        goTo: ->
            $el = $(@el)
            owl = $el.data 'carousel'
            thumb = $el.data 'thumb'

            $(owl).trigger('to.owl.carousel', parseInt(thumb - 1))

        goFromPage: ->
            $el = $(@el)
            popup = $el.data 'carousel-popup-target'
            index = $el.data 'index'
            $(popup)
                .find '#screens'
                .trigger 'to.owl.carousel', parseInt(index)
    
    PluginCreateCarousel = (option) ->
        @.each ->
            $this = $(@)
            data = $this.data('cl.createcarousel')

            if !data 
                $this.data('cl.createcarousel', (data = new CreateCarousel @))
            if (typeof option == 'string')
                data[option]()
            return

    PluginCarouselPopup = (option, value) ->
        @.each ->
            $this = $(@)
            data = $this.data('cl.CarouselPopup')

            if !data 
                $this.data('cl.CarouselPopup', (data = new CarouselPopup @))
            if (typeof option == 'string')
                data[option]()
            return



    $ ->
        initSmCarousel = (w, tr, $this) ->
            if w < tr
                PluginCreateCarousel.call($this, 'build')
            else
                PluginCreateCarousel.call($this, 'destroy')
            return
        
        destroyCarousel = (w, tr, $this) ->
            if w > tr
                PluginCreateCarousel.call($this, 'build')
            else
                PluginCreateCarousel.call($this, 'destroy')
            return

        $('.carousel').each ->

            $this = $(@)
            tr = $this.data 'destroy'

            if tr
                destroyCarousel($(window).width(), tr, $this)
                $(window).on 'resize', ->
                    destroyCarousel($(@).width(), tr, $this)
            else
                PluginCreateCarousel.call($this, 'build')
            
            return

        $('.must-be-carousel').each ->
            $this = $(@)
            tr = $this.data 'threshold'

            initSmCarousel($(window).width(), tr, $this)

            $(window).on 'resize', ->
                initSmCarousel($(@).width(), tr, $this)




    # ======================================================================== 

        sendGa = (index, string) ->
            console.log "#{string} #{index}" if typeof ga is 'function'
            ga("send", "event" ,"Content", "viewed", "#{string} #{index}") if typeof ga is "function"
            

        $('#carousel-heroes, #frames-carousel').on 'translated.owl.carousel', (e) ->
            string = $(@).data 'string'
            index = e.page.index + 1
            sendGa index, string

        
        $('#screens').on 'translate.owl.carousel', (e) ->
            string = $(@).data 'string'
            index = e.page.index + 1
            sendGa index, string        
        
        $('#first-frame').on 'touchend', (e) ->
            string = 'Dinosaur Still'
            index = 1
            ev = $(@).data 'max-event'

            if $(window).width() > ev
                sendGa index, string

    
    # =========================================================================

        screens = $('#screens')
        thumbs = $('#thumbs')

        $(document).on 'click', '[data-carousel-popup-target]', ->

            max = $(@).data 'max-event'

            if $(window).width() >= max 
                $this = $(@)
                target = $this.data 'carousel-popup-target'
                carousel = $(target).find('.carousel')

                PluginCarouselPopup.call($(target), 'showPopup')

                carousel.each ->
                    PluginCreateCarousel.call( $(@), 'destroy')
                    PluginCreateCarousel.call( $(@), 'build')
                    $(@).trigger('refresh.owl.carousel')

                
                PluginCarouselPopup.call($this, 'goFromPage')

       
        $(document).on 'click', '.overlay', ->
            PluginCarouselPopup.call($('.popup'), 'hidePopup')
        
        $(document).on 'click', '.js-close', ->
            PluginCarouselPopup.call($('.popup'), 'hidePopup')        
        
        $(document).on 'click', '[data-thumb]', ->
            PluginCarouselPopup.call($(@), 'goTo')


        flag = false
        duration = 300

        setActiveClass = (i) ->
            $('[data-thumb]').removeClass 'active'
            $('[data-thumb="'+i+'"]').addClass 'active'

        screens.on 'changed.owl.carousel', (e) ->
            if !flag
                flag = true
                thumbs.trigger('to.owl.carousel', [e.item.index, duration, true])
                setActiveClass(e.item.index + 1)
                flag = false

    return