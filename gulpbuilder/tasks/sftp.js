var fs = require('fs');
var file = fs.readFileSync('./sftp-conf.json');

if(!file) return

var conf = JSON.parse(file);

var gulp = require('gulp');
var sftp = require('gulp-sftp');
var changed = require('gulp-changed');

var folder = 'dist/**/**/**/*';

gulp.task('sftp', function () {
    return gulp.src(folder)
    	.pipe(changed(folder, {hasChanged: changed.compareLastModifiedTime}))
        .pipe(sftp({
            host: conf.host,
            user: conf.user,
            pass: conf.password,
            remotePath: conf.remote_path
        }));
});